﻿#region Namespaces

using il2_guigen.Properties;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

#endregion

namespace il2_guigen
{
    public class Program
    {
        #region MAIN

        public static void Main(string[] args)
        {
            Initialize();
            ValidateAndParseInput(args);
            GenerateModel();
            Finish();
        }

        #endregion

        #region EventHandlers
        
        private static void UnexpectedErrorHandler(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;

            if (ex is ApplicationException)
            {
                if (ex.Message == "USAGE")
                    ShowUsage();
            }
            else
                Console.WriteLine("Error: " + ex.Message);

            Finish();
        }

        #endregion

        #region Helpers

        private static void GenerateModel()
        {
            decimal width = ((decimal)_width) / 100;
            decimal height = ((decimal)_height) / 100;
            string asciiData = string.Empty;

            if (_type == "GUI")
                asciiData = Resources.GuiBase;
            else if (_type == "LAYOUT")
            {
                asciiData = Resources.LayoutBase;

                // we have scale = 1.5, and we need 75% size to correctly resize glow effect
                //width *= 0.75m;
                //height *= 0.75m;
            }

            string radians = AngleToRadians(_angle);
            string vertex = "";
            decimal widthGui = width / 2;
            decimal heightGui = height / 2;

            var sbVertex = new StringBuilder();

            sbVertex.AppendFormat("#### Dimensions: Width: {0}, Height: {1} pixels\n", _width, _height);
            sbVertex.AppendFormat("  {0}\n", "verts 4");
            sbVertex.AppendFormat("    {0} {1} {2}\n", -widthGui, -heightGui, 0);
            sbVertex.AppendFormat("    {0} {1} {2}\n", widthGui, -heightGui, 0);
            sbVertex.AppendFormat("    {0} {1} {2}\n", -widthGui, heightGui, 0);
            sbVertex.AppendFormat("    {0} {1} {2}", widthGui, heightGui, 0);

            vertex = sbVertex.ToString();

            asciiData = asciiData.Replace("<NAME>", _name);
            asciiData = asciiData.Replace("<ANGLE>", radians + " # Angle: " + _angle + " degree");
            asciiData = asciiData.Replace("<VERTS>", vertex);
            
            File.WriteAllText(@".\" + _name + ".mdl", asciiData);
        }

        private static void Initialize()
        {
            AppDomain.CurrentDomain.UnhandledException += UnexpectedErrorHandler;
        }

        private static void ShowUsage()
        {
            Console.WriteLine();
            Console.WriteLine("***** USAGE *****");
            Console.WriteLine("il2_guigen.exe <gui|layout> <name> <width> <height> <angle>");
            Console.WriteLine("Example:");
            Console.WriteLine("il2_guigen.exe gui my_sample_window 600 800 60");
            Console.WriteLine("  Generates the following 3d model");
            Console.WriteLine("  TYPE = GUI MODEL");
            Console.WriteLine("  NAME = \"my_sample_window\"");
            Console.WriteLine("  WIDTH = \"600 pixels\"");
            Console.WriteLine("  HEIGHT = \"800 pixels\"");
            Console.WriteLine("  ORIENTATION = \"60 degree\"");
            Console.WriteLine();
            Console.WriteLine("il2_guigen.exe layout menu_choice 400 60 80");
            Console.WriteLine("  Generates the following 3d model");
            Console.WriteLine("  TYPE = LAYOUT MODEL");
            Console.WriteLine("  NAME = \"menu_choice\"");
            Console.WriteLine("  WIDTH = \"400 pixels\"");
            Console.WriteLine("  HEIGHT = \"60 pixels\"");
            Console.WriteLine("  ORIENTATION = \"80 degree\"");
            Console.WriteLine();
        }

        private static void Finish()
        {
            if (Debugger.IsAttached)
            {
                Console.WriteLine();
                Console.WriteLine("*** Press ENTER to exit ***");
                Console.ReadLine();
            }

            Environment.Exit(0);
        }

        private static void ValidateAndParseInput(string[] args)
        {
            if (args == null)
                throw new ArgumentException("args");
            
            else if (args.Length < 5)
                throw new ApplicationException("USAGE");

            _type = args[0].ToUpper();

            if (_type != "GUI" && _type != "LAYOUT")
                throw new ArgumentException("Invalid model type " + args[0]);

            _name = args[1];

            if (_name.Length > 16)
                throw new ArgumentException("Name's maximum length is 16 characters long");

            if (_name.Contains(" "))
                throw new ArgumentException("Name cannot contain spaces");

            if (!int.TryParse(args[2], out _width))
                throw new ArgumentException("Unable to parse width value from " + args[1]);

            if (!int.TryParse(args[3], out _height))
                throw new ArgumentException("Unable to parse height value from " + args[2]);

            if (!int.TryParse(args[4], out _angle))
                throw new ArgumentException("Unable to parse angle value from " + args[3]);
        }

        private static string AngleToRadians(int angle)
        {
            return Math.Round(((decimal)angle) * (decimal)(Math.PI / 180.0f), 4).ToString();
        }

        #endregion

        #region Fields

        private static int _width = 0;
        private static int _height = 0;
        private static int _angle = 0;
        private static string _name = string.Empty;
        private static string _type = string.Empty;

        #endregion
    }
}

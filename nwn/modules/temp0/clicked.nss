void main()
{
    object pc = GetPlaceableLastClickedBy();
    string param = GetLocalString(OBJECT_SELF, "GUI_LAYOUT");

    SendMessageToPC(pc, "layout: " + param);
    SetLocalInt(pc, "clickParam", StringToInt(param));
    ExecuteScript("chat_script", pc);
    SetLocalInt(pc, "clickParam", 0);


}

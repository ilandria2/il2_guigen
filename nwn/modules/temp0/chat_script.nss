#include "vg_s0_gui_core_c"

float optionHeight = 0.74; //524 x 74 px
const float camOffsetAngle = 0.0f;//-2.2f;
const float camOffsetDistance = 0.0f;//-0.6f;
void Help(object pc);
void DestroyNear(object pc);



void main()
{
    object pc = GetPCChatSpeaker();
    if (pc == OBJECT_INVALID) pc = OBJECT_SELF;
    SendMessageToPC(pc, "____");

    int clickParam = GetLocalInt(pc, "clickParam");
    SendMessageToPC(pc, "____clickParam: " + IntToString(clickParam));
    string msg = GetPCChatMessage();
    int spacePos = FindSubString(msg, " ");

    if (spacePos == -1 && !clickParam)
    {
        Help(pc);
        return;
    }

    string cmd = GetSubString(msg, 0, spacePos);
    string param = GetSubString(msg, spacePos + 1, 9999);
    cmd = GetStringUpperCase(cmd);

    if (cmd == "M") // mouse click simulation
    {/*
        int type = StringToInt(param);

        if (type == 0)
        {
            AssignCommand(pc, SetCameraFacing(GUI_CAMERA_DIRECTION, GUI_CAMERA_DISTANCE, GUI_CAMERA_ANGLE));
            SetCameraHeight(pc, GUI_CAMERA_HEIGHT);

            // mouse click position
            vector pos = GetPosition(pc);
            pos = GetChangedPosition(pos, 7.35, GUI_CAMERA_DIRECTION);
            pos = Vector(pos.x, pos.y, pos.z + 2.45);
            location locFront = Location(GetArea(pc), pos, 0.0f);

            // camera position
            pos = GetPosition(pc);
            pos.z += GUI_CAMERA_HEIGHT;

            float camDist = (GUI_CAMERA_DISTANCE + camOffsetDistance) * sin(GUI_CAMERA_ANGLE + camOffsetAngle);

            pos = GetChangedPosition(pos, camDist, GetOppositeDirection(GUI_CAMERA_DIRECTION));
            float camUp = sqrt(pow(GUI_CAMERA_DISTANCE + camOffsetDistance, 2.0f) + pow(camDist, 2.0f));

            pos.z = camUp;
            location locCam = Location(GetArea(pc), pos, 0.0f);

            //SendMessageToPC(pc, "Front pos: " + VectorToString(GetPositionFromLocation(locFront)));
            SendMessageToPC(pc, "Cam pos: " + VectorToString(GetPositionFromLocation(locCam)));
            SendMessageToPC(pc, "Cam up offset: " + FloatToString(camUp));
            // apply
            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEAD_FIRE), locFront);
            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEAD_EVIL), locCam);

            //vector posPc = GetPosition(pc);
            //ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_FLAME_S), Location(GetArea(pc), Vector(posPc.x, posPc.y, posPc.z + 1.0), 0.0f));
            //ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEAD_SONIC), Location(GetArea(pc), Vector(posPc.x, posPc.y, posPc.z + 5.0), 0.0f));
            //ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(VFX_IMP_HEAD_EVIL), Location(GetArea(pc), Vector(posPc.x, posPc.y, posPc.z + 10.0), 0.0f));
        }
        */
    }

    else if (clickParam > 0 || cmd == "MENU")
    {


        int gui = StringToInt(param);
        int layer = 0;
        int row = 0;
        float side = 1.5f;
        float heightStart = optionHeight * 2;

        if (clickParam)
        {
            clickParam -= 8200;

            if (clickParam == 15)
                GUI_StopMenu(pc);
            else
                SendMessageToPC(pc, "Choice " + IntToString(clickParam));

            return;
        }

        if (gui == 0)
        {
            GUI_AddObject(pc, xWindow, 0.0f, 0.0f, layer++);
            GUI_AddObject(pc, 8199, 0.025, 2.015f, layer);
            GUI_AddObject(pc, 8215, 2.07, 3.65f, layer); // close
            GUI_AddObject(pc, 8216, 1.825, -2.15f, layer); // title

            int i;
            for (; i < 9; i++)
            {
                vector gridPos = GUI_OffsetGrid(100.0f, 100.0f, -450.0f, -300.0f, 60.0f, 73.0f, 3, 3, 10.0f, i + 1);
                GUI_AddObject(pc, 8200 + i, gridPos.y, gridPos.x, layer);
            }

            GUI_RenderMenu(pc);
            SetLocalInt(GetFirstPC(), "SELECTOR", TRUE);
        }

        else if (gui == 1)
        {
            GUI_RemoveObject(pc, 3);
            GUI_RemoveObject(pc, 2);
            GUI_RemoveObject(pc, 1);
            layer = 1;
            GUI_AddObject(pc, xChoice1, heightStart - row++ * optionHeight, side, layer);
            GUI_AddObject(pc, xChoice4, heightStart - row++ * optionHeight, side, layer);
            GUI_RenderMenu(pc);
        }

        else if (gui == 2)
        {
            GUI_RemoveObject(pc, 2);
            GUI_RemoveObject(pc, 1);
            layer = 1;
            GUI_AddObject(pc, xChoice1, heightStart - row++ * optionHeight, side, layer);
            GUI_AddObject(pc, xChoice4, heightStart - row++ * optionHeight, side, layer);
            GUI_AddObject(pc, xChoice2, heightStart - row++ * optionHeight, side, layer);
            GUI_AddObject(pc, xChoice5, heightStart - row++ * optionHeight, side, layer);
            GUI_RenderMenu(pc);
        }

        else if (gui == 9)
        {
            GUI_StopMenu(pc);
            SetLocalInt(pc, "DEBUG", TRUE);
        }
    }

    else if (cmd == "GUI")
    {
        int gui = StringToInt(param);

        if (gui == 0)
        {
            SendMessageToPC(pc, "invalid gui param: " + param);
            return;
        }

        location loc = GetLocation(pc);
        int layer = 0;

        GUI_LockCamera(pc);
        DelayCommand(GUI_LIFETIME, GUI_UnlockCamera(pc));

        // sample indicator
        if (gui == 1)
        {
            SendMessageToPC(pc, "Testing sample indicator...");
            ApplyEffectAtLocation(DURATION_TYPE_INSTANT, EffectVisualEffect(8195), loc);
        }

        // sample window
        else if (gui == 2)
        {
            int row = -2;

            SendMessageToPC(pc, "Testing sample window...");
            GUI_RenderObject(xWindow, pc, 0.0f, 0.0f, layer++);
            //GUI_RenderObject(xChoice1, pc, row++ * optionHeight, row * 1.0f, layer);
            //GUI_RenderObject(xChoice2, pc, row++ * optionHeight, row * 1.0f, layer);
            //GUI_RenderObject(xChoice3, pc, row++ * optionHeight, row * 1.0f, layer);
            //GUI_RenderObject(xChoice4, pc, row++ * optionHeight, row * 1.0f, layer);
            //GUI_RenderObject(xChoice5, pc, row++ * optionHeight, row * 1.0f, layer);
        }

        // sample window 2
        else if (gui == 3)
        {
            int row = -2;
            SendMessageToPC(pc, "Testing sample window...");
            GUI_RenderObject(xWindow, pc, 0.0f, 0.0f, layer++);
            GUI_RenderObject(xChoice1, pc, row++ * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice2, pc, row++ * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice3, pc, row++ * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice4, pc, row++ * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice5, pc, row++ * optionHeight, 0.0f, layer);
        }

        // sample window 3
        else if (gui == 4)
        {
            int row = 2;

            SendMessageToPC(pc, "Testing sample window...");
            GUI_RenderObject(xWindow, pc, 0.0f, 0.0f, layer++);
            GUI_RenderObject(xChoice1, pc, row-- * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice2, pc, row-- * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice3, pc, row-- * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice4, pc, row-- * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice5, pc, row-- * optionHeight, 0.0f, layer);
        }

        // sample window 4
        else if (gui == 5)
        {
            int row = 0;

            SendMessageToPC(pc, "Testing sample window...");
            GUI_RenderObject(xWindow, pc, 0.0f, 0.0f, layer++);
            GUI_RenderObject(xChoice1, pc, row++ * optionHeight, 0.0f, layer);
            GUI_RenderObject(xChoice2, pc, row++ * optionHeight, 0.0f, layer);
        }
    }

    else if (cmd == "VFX")
    {
        int vfx = StringToInt(param);

        if (vfx == 0)
        {
            SendMessageToPC(pc, "invalid vfx param: " + param);
            return;
        }

        location loc = GetLocation(pc);
        vector pos = GetPositionFromLocation(loc);
        float angle = GetFacingFromLocation(loc);
        pos = Vector(pos.x, pos.y, pos.z + GUI_HEIGHT);
        loc = Location(GetArea(pc), pos, angle);

        SendMessageToPC(pc, "Applying effect: " + IntToString(vfx));
        ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectVisualEffect(vfx), loc, 15.0f);
    }

    else if (cmd == "AOE")
    {
        int aoe = StringToInt(param);

        if (aoe == 0)
        {
            SendMessageToPC(pc, "invalid aoe param: " + param);
            return;
        }

        location loc = GetLocation(pc);
        vector pos = GetPositionFromLocation(loc);
        float angle = GetFacingFromLocation(loc);
        pos = Vector(pos.x, pos.y, pos.z + 3.0f);
        loc = Location(GetArea(pc), pos, angle);

        SendMessageToPC(pc, "Applying aoe effect: " + IntToString(aoe));
        ApplyEffectAtLocation(DURATION_TYPE_TEMPORARY, EffectAreaOfEffect(aoe), loc, 10.0f);
        DelayCommand(5.0f, DestroyNear(pc));
    }

    else if (cmd == "DIRECTION")
    {
        AssignCommand(pc, SetCameraFacing(StringToFloat(param)));
        SendMessageToPC(pc, "Camera direction = " + param);
    }

    else if (cmd == "DISTANCE")
    {
        AssignCommand(pc, SetCameraFacing(-1.0f, StringToFloat(param)));
        SendMessageToPC(pc, "Camera distance = " + param);
    }

    else if (cmd == "PITCH")
    {
        AssignCommand(pc, SetCameraFacing(-1.0f, -1.0f, StringToFloat(param)));
        SendMessageToPC(pc, "Camera pitch = " + param);
    }

    else if (cmd == "HEIGHT")
    {
        SetCameraHeight(pc, StringToFloat(param));
        SendMessageToPC(pc, "Camera pitch = " + param);
    }

    else
    {
        Help(pc);
    }
}


void Help(object pc)
{
        string help = "Commands";
        help += "\nVFX num (visualeffects.2da row)";
        help += "\nAOE num (vfx_persistent.2da row)";
        help += "\nDIRECTION num (camera direction 0-360)";
        help += "\nDISTANCE num (camera distance 1-25)";
        help += "\nPITCH num (camera pitch 1-89)";
        help += "\nHEIGHT num (camera height (no limit))";
        help += "\n*** camera settings are specific to camera modes ***";

        help += "\n\nExamples:";
        help += "\nvfx 50 >> creates visual effect id 50";
        help += "\ndirection 90 >> 90 degree angle";
        help += "\ndistance 5 >> 5 meters away";
        help += "\npitch 10 >> 10 pitch top-down angle";
        help += "\nheight 4 >> 4 meters high";
        SendMessageToPC(pc, help);
}

void DestroyNear(object pc)
{
    int n = 0;
    for (n = 0; n < 3; n++)
    {
        object o = GetNearestObject(OBJECT_TYPE_AREA_OF_EFFECT, pc, n);
        if (GetIsObjectValid(o))
        {
            SendMessageToPC(pc, "Destroying AOE id " + IntToString(n) + " Tag:" + GetTag(o) + " Name: " + GetName(o));
            SetPlotFlag(o, FALSE);
            DestroyObject(o, 0.1);
        }
    }
}

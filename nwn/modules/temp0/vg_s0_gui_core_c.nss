#include "x0_i0_position"

const int xChoice1 = 8193;
const int xChoice2 = 8194;
const int xChoice3 = 8196;
const int xChoice4 = 8197;
const int xChoice5 = 8198;
const int xWindow = 8192;

const float GUI_CAMERA_ANGLE = 70.0f;
const float GUI_CAMERA_HEIGHT = 5.0f;
const float GUI_CAMERA_DISTANCE = 10.0f;
const float GUI_CAMERA_DIRECTION = DIRECTION_NORTH;
const float GUI_HEIGHT = 5.5f;
const float GUI_LIFETIME = 3.001f;
const float GUI_LAYER_THICKNESS = 0.001f;
const float GUI_LAYOUT_Z_OFFSET = -1.2f;
const int GUI_LAYOUT_LAYER_MODIFIER = 5;

// Returns pixels / 100
float PixelsToMeters(float pixels);

// calculates XY offset based on the Z coordinate and angle of the model's orientation
// if degAngle = -1 then 50 degree is used
vector GUI_Offset(vector pos, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE);

// renders gui vfx at locRender adjusted by height and side offsets for a specific layerLevel and degAngle mdl orientation
void GUI_RenderObject(int xGui, object pc, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE);

// menu rendering loop
void GUI_RenderMenu(object pc);

// enters menu rendering loop if not in any yet
void GUI_RenderMenuSequence(object pc, int id = 0);

// Sets layer level for a specific gui object row
void GUI_SetLayer(object pc, int guiRow = 1, int level = 0);

// Gets layer level for a specific gui object row
int GUI_GetLayer(object pc, int guiRow = 1);

// Sets height position for a specific gui object row
void GUI_SetHeight(object pc, int guiRow = 1, float guiHeight = 0.0f);

// Gets height position for a specific gui object row
float GUI_GetHeight(object pc, int guiRow = 1);

// Sets side position for a specific gui object row
void GUI_SetSide(object pc, int guiRow = 1, float guiSide = 0.0f);

// Gets side position for a specific gui object row
float GUI_GetSide(object pc, int guiRow = 1);

// Sets the max layer level
void GUI_SetLayerMax(object pc, int level = 0);

// Gets the max layer level
int GUI_GetLayerMax(object pc);

// Sets gui object for a specific gui object row
void GUI_SetObject(object pc, int guiObject, int guiRow = 0);

// Gets gui object for a specific gui object row
int GUI_GetObject(object pc, int guiRow = 0);

// Gets the amount of objects of the current player's menu
int GUI_GetObjectCount(object pc);

// Gets the amount of objects of the current player's menu
void GUI_SetObjectCount(object pc, int guiRows = 0);

// Sets rendering state for player
void GUI_SetState(object pc, int state = TRUE);

// Gets rendering state for player
int GUI_GetState(object pc);

// Gets the current sequence id for player
int GUI_GetSequence(object pc);

// Sets the current sequence id for player
// -1 will use current id + 1 instead
void GUI_SetSequence(object pc, int id = 0);

// Adds guiObject on the next available row on a specific height x side x layer coordinates
void GUI_AddObject(object pc, int guiObject, float fHeight, float fSide, int layer);

// Removes object at specific row
void GUI_RemoveObject(object pc, int guiRow = 0);

// Locks player's camera and movement
void GUI_LockCamera(object player);

// Unlocks player's camera and movement
void GUI_UnlockCamera(object player);

// Stops menu mode for player
void GUI_StopMenu(object player);

// Gets offset position (result with vector.x, vector.y) for gui object by given gui size, bottom-left corner position, distance between corner and gui, grid size, margin and top-left based index)
// ** INPUTS IN PIXELS
vector GUI_OffsetGrid(float guiWidth, float guiHeight, float cornerX, float cornerY, float cornerDistX, float cornerDistY, int gridX, int gridY, float gridMargin, int gridIndex);

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void GUI_RenderMenu(object pc)
{
    if (GUI_GetState(pc))
        GUI_RenderMenuSequence(pc, -1);
    else
    {
        GUI_SetState(pc, TRUE);
        GUI_RenderMenuSequence(pc);
    }
}

void GUI_RenderMenuSequence(object pc, int id = 0)
{
    //SpawnScriptDebugger();
SendMessageToPC(pc, "-----------------------------------");
SendMessageToPC(pc, "SEQ(" + IntToString(id) + " started");

    int guiObjects = GUI_GetObjectCount(pc);
    if (guiObjects == 0)
    {
        SendMessageToPC(pc, "SEQ object count 0 - exit");
        GUI_SetState(pc, FALSE);
        return;
    }

    GUI_SetState(pc, TRUE);

    int seqId = GUI_GetSequence(pc);
    if (seqId > id && id != -1) // seq triggered before loop
    {
SendMessageToPC(pc, "SEQ old return (" + IntToString(seqId) + ")");
        return;
    }

    GUI_SetSequence(pc, id);

    int guiRow, guiObject, guiLayer, baseLayer;
    float guiHeight, guiSide;

    if (id == -1)
    {
        baseLayer = GUI_GetLayerMax(pc) + 1;
        id = GUI_GetSequence(pc);
SendMessageToPC(pc, "SEQ manual - baseLayer (" + IntToString(baseLayer) + ") id (" + IntToString(id) + ")");
    }

    for (; guiRow <= guiObjects; guiRow++)
    {
        guiObject = GUI_GetObject(pc, guiRow);

        if (guiObject > 0)
        {
            guiHeight = GUI_GetHeight(pc, guiRow);
            guiSide = GUI_GetSide(pc, guiRow);
            guiLayer = GUI_GetLayer(pc, guiRow);
            guiLayer += baseLayer;

SendMessageToPC(pc, "SEQ rendering (" + IntToString(guiRow) + ":" + IntToString(guiObject) + ") layer (" + IntToString(guiLayer) + ")");
            GUI_RenderObject(guiObject, pc, guiHeight, guiSide, guiLayer);
        }
    }

    // TODO: implement timer - if sequence was triggered manually before GUI_LIFETIME
    // then ignore this thread

SendMessageToPC(pc, "SEQ delay menu seq (" + IntToString(id) + ")");
    DelayCommand(GUI_LIFETIME - 0.05, GUI_RenderMenuSequence(pc, id));
}

int GUI_GetState(object pc)
{
    return GetLocalInt(pc, "GUI_STATE");
}

void GUI_SetState(object pc, int state = TRUE)
{
    if (GUI_GetState(pc) == state)
        return;

    if (state)
        GUI_LockCamera(pc);
    else
        GUI_UnlockCamera(pc);

    SetLocalInt(pc, "GUI_STATE", state);
}

void GUI_SetObject(object pc, int guiObject, int guiRow = 0)
{
    int guiRows = GUI_GetObjectCount(pc);

    SetLocalInt(pc, "GUI_OBJECT_" + IntToString(guiRow), guiObject);

    if (guiRow > guiRows - 1)
        GUI_SetObjectCount(pc, guiRow + 1);
}

int GUI_GetObject(object pc, int guiRow = 0)
{
    return GetLocalInt(pc, "GUI_OBJECT_" + IntToString(guiRow));
}

int GUI_GetObjectCount(object pc)
{
    return GetLocalInt(pc, "GUI_OBJECT_COUNT");;
}

void GUI_SetObjectCount(object pc, int guiRows = 0)
{
    SetLocalInt(pc, "GUI_OBJECT_COUNT", guiRows);
}

void GUI_SetSide(object pc, int guiRow = 1, float guiSide = 0.0f)
{
    SetLocalFloat(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_SIDE", guiSide);
}

float GUI_GetSide(object pc, int guiRow = 1)
{
    return GetLocalFloat(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_SIDE");
}

void GUI_SetHeight(object pc, int guiRow = 1, float guiHeight = 0.0f)
{
    SetLocalFloat(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_HEIGHT", guiHeight);
}

float GUI_GetHeight(object pc, int guiRow = 1)
{
    return GetLocalFloat(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_HEIGHT");
}

void GUI_SetLayer(object pc, int guiRow = 1, int level = 0)
{
    int maxLevel = GUI_GetLayerMax(pc);
    SetLocalInt(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_LAYER", level);

    if (level > maxLevel)
        GUI_SetLayerMax(pc, level);
}

void GUI_SetLayerMax(object pc, int level = 0)
{
    SetLocalInt(pc, "GUI_LAYER", level);
}

int GUI_GetLayerMax(object pc)
{
    return GetLocalInt(pc, "GUI_LAYER");
}

int GUI_GetLayer(object pc, int guiRow = 1)
{
    return GetLocalInt(pc, "GUI_OBJECT_" + IntToString(guiRow) + "_LAYER");
}

int GUI_GetSequence(object pc)
{
    return GetLocalInt(pc, "GUI_SEQUENCE");
}

void GUI_SetSequence(object pc, int id = 0)
{
    int seqId = GUI_GetSequence(pc);

    if (id > 0)
        seqId = id;

    else if (id == -1)
        seqId++;

SendMessageToPC(pc, "SEQ setting seq to (" + IntToString(seqId) + ")");
    SetLocalInt(pc, "GUI_SEQUENCE", seqId);
}

void GUI_AddObject(object pc, int guiObject, float fHeight, float fSide, int layer)
{
    int rows = GUI_GetObjectCount(pc);
    rows;

    GUI_SetHeight(pc, rows, fHeight);
    GUI_SetSide(pc, rows, fSide);
    GUI_SetLayer(pc, rows, layer);
    GUI_SetObject(pc, guiObject, rows);
}

void GUI_RemoveObject(object pc, int guiRow = 0)
{
    int rows = GUI_GetObjectCount(pc);
    int row, layer, guiObject;
    float fHeight, fSide;
    for (row = guiRow; row < rows - 1; row++)
    {
        fHeight = GUI_GetHeight(pc, row + 1);
        fSide = GUI_GetSide(pc, row + 1);
        layer = GUI_GetLayer(pc, row + 1);
        guiObject = GUI_GetObject(pc, row + 1);

        GUI_SetHeight(pc, row, fHeight);
        GUI_SetSide(pc, row, fSide);
        GUI_SetLayer(pc, row, layer);
        GUI_SetObject(pc, guiObject, row);
    }

    GUI_SetHeight(pc, row);
    GUI_SetSide(pc, row);
    GUI_SetLayer(pc, row);
    GUI_SetObject(pc, 0, row);
    GUI_SetObjectCount(pc, rows - 1);
}

void GUI_LockCamera(object player)
{
    AssignCommand(player, SetCameraFacing(GUI_CAMERA_DIRECTION, GUI_CAMERA_DISTANCE, GUI_CAMERA_ANGLE));
    AssignCommand(player, LockCameraDirection(player, TRUE));
    AssignCommand(player, LockCameraPitch(player, TRUE));
    SetCameraHeight(player, GUI_CAMERA_HEIGHT);
    SetCommandable(FALSE, player);
}

void GUI_UnlockCamera(object player)
{
    SetCameraHeight(player, 0.0f);
    SetCommandable(TRUE, player);
    LockCameraDirection(player, FALSE);
    LockCameraPitch(player, FALSE);
}

void GUI_RenderObject(int xGui, object pc, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE)
{
    vector pos = GetPosition(pc);
    pos = Vector(pos.x, pos.y, pos.z + GUI_HEIGHT);
    location locTarget = Location(GetArea(pc), GUI_Offset(pos, height, side, layerLevel, degAngle), 0.0f);

    ApplyEffectAtLocation(
        DURATION_TYPE_INSTANT,
        EffectVisualEffect(xGui),
        locTarget
    );

    vector posi = GetPositionFromLocation(locTarget);
    posi.z += GUI_LAYOUT_Z_OFFSET;

    if (GetLocalInt(GetFirstPC(), "SELECTOR") || xGui == xWindow) return;
    object selector;

    if (xGui >= 8200 && xGui <= 8214)
        selector = CreateObject(OBJECT_TYPE_PLACEABLE, "CHOICE", Location(GetArea(pc), posi, GUI_CAMERA_DIRECTION));
    else if (xGui == 8215)
        selector = CreateObject(OBJECT_TYPE_PLACEABLE, "CLOSE", Location(GetArea(pc), posi, GUI_CAMERA_DIRECTION));

    SetLocalString(selector, "GUI_LAYOUT", IntToString(xGui));
    SetName(selector, "</c><cddd>" + "[ " + IntToString(xGui) + " ]</c>");
}

void GUI_StopMenu(object player)
{
    int guiRows = GUI_GetObjectCount(player);
    int guiRow;
    for (; guiRow < guiRows; guiRow++)
    {
        DeleteLocalFloat(player, "GUI_HEIGHT_" + IntToString(guiRow));
        DeleteLocalFloat(player, "GUI_SIDE_" + IntToString(guiRow));
        DeleteLocalInt(player, "GUI_LAYER_" + IntToString(guiRow));
        DeleteLocalInt(player, "GUI_OBJECT_" + IntToString(guiRow));
    }

    DeleteLocalInt(player, "GUI_OBJECT_COUNT");
    DeleteLocalInt(player, "GUI_SEQUENCE");
    GUI_SetState(player, FALSE);
    DeleteLocalInt(player, "SELECTOR");
}

vector GUI_Offset(vector pos, float height, float side, int layerLevel = 0, float degAngle = GUI_CAMERA_ANGLE)
{
    /*

       (targetDist)

        C   a    B
         x------x
          \     |
           \    |
            \   | c (targetZ)
  (height) b \  |
              \ |
               \|
                x
                A
    beta= 90
    gama = 70
    alfa = 180-90-70 = 20

        a            b
    ---------  = ----------
    sin(alfa)     sin(beta)

         b * sin(alfa)
    a = ----------------
            sin(beta)

    b^2 = a^2 + c^2
    c^2 = b^2 - a^2
    c = square_root(b^2 - a^2)

    */


    float targetDist = 0.0f;
    float targetZ = 0.0f;
    float degBeta = 90.0f;
    float degAlfa = degBeta - degAngle;

    targetDist = (height) * sin(degAlfa) / sin(degBeta);
    targetDist -= GUI_LAYER_THICKNESS + layerLevel * GUI_LAYER_THICKNESS;
    targetZ = sqrt(pow(height, 2.0) - pow(targetDist, 2.0));

    if (height < 0.0f) targetZ = -targetZ;

    vector targetPos = Vector(pos.x, pos.y, pos.z + targetZ);
    targetPos = GetChangedPosition(targetPos, targetDist, 90.0f);
    targetPos = GetChangedPosition(targetPos, side, 0.0f);
    return targetPos;
}

float PixelsToMeters(float pixels)
{
    return pixels / 100.0f;
}

vector GUI_OffsetGrid(float guiWidth, float guiHeight, float cornerX, float cornerY, float cornerDistX, float cornerDistY, int gridX, int gridY, float gridMargin, int gridIndex)
{
    guiWidth = PixelsToMeters(guiWidth);
    guiHeight = PixelsToMeters(guiHeight);
    cornerX = PixelsToMeters(cornerX);
    cornerY = PixelsToMeters(cornerY);
    cornerDistX = PixelsToMeters(cornerDistX);
    cornerDistY = PixelsToMeters(cornerDistY);
    gridMargin = PixelsToMeters(gridMargin);

    //=IF(MOD(Id,Gridx)=0,Gridx-1,MOD(Id,Gridx)-1)
    int offsetX = gridIndex % gridX;
    if (offsetX == 0) offsetX = gridX;
    offsetX -= 1;

    //=Gridy-ROUNDDOWN((Id-1)/Gridy,0)-1
    int offsetY = gridY - (gridIndex - 1) / gridY - 1;

    //=CBLx + Dx + Gx/2 + OffsetX * Gx + OffsetX * Gridm
    float guiX = cornerX + cornerDistX + guiWidth / 2 + offsetX * guiWidth + offsetX * gridMargin;

    //=CBLy + Dy + Gy/2 + OffsetY * Gy + OffsetY * Gridm
    float guiY = cornerY + cornerDistY + guiHeight / 2 + offsetY * guiHeight + offsetY * gridMargin;

    return Vector(guiX, guiY, 0.0f);
}


